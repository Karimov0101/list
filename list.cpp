#include "list.h"
List::Node::Node(int value)
	: value(value),
	next(nullptr) {}

List::Node::~Node() {
}

List::List()
	: head_(nullptr) {}

List::~List() {
	int g = m_razmer;
	while (g > 0)
	{
		Node* element = head_;
		head_ = head_->next;
		delete element;
		g--;
	}
}

void List::add(int value, int index) {
	if (head_ == nullptr) {
		head_ = new Node(value);
		++m_razmer;
		return;
	}
	if (index == 0)
	{
		Node *new_obj = new Node(value);
		new_obj->next = head_;
		head_ = new_obj;
		++m_razmer;
		return;
	}
	Node* current = head_;
	int g = index-1;
	while (current->next && g > 0) {
		current = current->next;
		g--;
	}
	Node* new_node = new Node(value);
	new_node->next = current->next;
	current->next = new_node;
	++m_razmer;
}
void List::remove(int index)
{
	if ((index < 0) || (index > m_razmer-1))
	{
		return;
	}
	if (index == 0)
	{
		Node* current = head_;
		head_ = head_->next;
		delete current;
		current = nullptr;
	}
	else
	{
		Node* current = head_;
		int g = index - 1;
		while (current->next && g > 0) {
			current = current->next;
			g--;
		}
		Node* element = current->next;
		current->next = element->next;
		delete element;
		element = nullptr;
	}
	--m_razmer;
}
void List::add(int value) {
	if (head_ == nullptr) {
		head_ = new Node(value);
		++m_razmer;
		return;
	}
	Node* current = head_;
	while (current->next) {
		current = current->next;
	}
	Node* new_node = new Node(value);
	current->next = new_node;
	m_razmer++;
}
int List::get(int index)
{
	int i = 0;
	Node *current = head_;
	while (i<=m_razmer)
	{
		if (i == index)
		{
			return current->value;
		}
		current = current->next;
		i++;
	}
}
int List::razmer() { return m_razmer; }

