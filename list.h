#pragma once
class List {
private:
	int m_razmer=0;
public:
	List();
	~List();

	void add(int value, int index);
	void add(int value);
	void remove(int index);
	int razmer();
	int get(int index);

private:
	class Node {
	public:
		Node(int value);
		~Node();
		int value;
		Node* next;
	};
	Node* head_;
};
