#include <iostream>
#include<cassert>
#include "list.h"

using namespace std;

void test1() {
	List l;
	l.add(1);
	l.add(2);
	int g = 1;
	for (size_t i = 0; i < 2; i++)
    {
		assert(l.get(i) == g);
		g++;
    }
	cout << '\n' <<"First element of list(used get()): ";
	cout << l.get(0);
	assert(l.get(1) == 2);
}
void test2()
{
	List h;
	h.add(1, 0);
	h.add(2, 1);
	h.add(3, 2);
	h.add(4, 3);
	h.add(5, 4);
	h.add(7, 2);
	h.add(9, 6);
	h.add(8, 7);
	h.add(10, 0);

	assert(h.get(3) == 7);
	assert(h.get(0) == 10);

	for (size_t i = 0; i < 9; i++)
		cout << h.get(i)<< ' ';

	cout << '\n' << "Second element of list(used: get()): ";
	cout << h.get(8);
	assert(h.get(8) == 8);
}
void test3()
{
	List j;
	j.add(1);
	j.add(2);
	j.add(3);
	j.add(4);
	int g = 1;

	for (size_t i = 0; i < j.razmer(); i++)
	{
		assert(j.get(i) == g);
		g++;
	}
	cout << "size before :" << j.razmer() << '\n';

	cout << "Delete element with index(0):" << '\n';
	j.remove(0);
	assert(j.razmer() == 3);

	cout << "Delete element with index (2):" << '\n';
	j.remove(2);
	assert(j.razmer() == 2);

	cout << "Size after: " << j.razmer() << '\n'
		 << "elements: ";
	g = 2;
	for (size_t i = 0; i < j.razmer(); i++)
	{
		cout << j.get(i) << ' ';
		assert(j.get(i) == g);
		g++;
	}
}

int main() {
	cout << "TEST 1 " << '\n';
	test1(); // testing add(int x) + [] + get()
	std::cout << '\n';
	cout << "TEST 2 " << '\n';
	test2(); // testing add(int x , int index) + [] + get()
	std::cout << '\n';
	cout << "TEST 3 " << '\n';
	test3(); // testing remove(int x, int index) + [] + get()
}
